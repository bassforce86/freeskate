UI = function (_game, _debug) {

    this.game = _game; //game reference
    this.debug = _debug; //debug reference

    var resetTimer = 0, //time between resets
        levelReset,
        uiPush, //UI push Text
        uiScore, // UI score text
        uiBack, //ui background reference
        uiRstBack, //ui reset background reference
        uiMenuBack, //ui menu background
        uiTrick; //game display text

    // Center ui text
    var pushText = "Pushes",
        scoreText = "Score: " + this.score,
        style = { font: "20px Fontdiner Swanky", fill: "#999", align: "center" },
        width = 465,
        height = 2;

    // Declarations
    this.uiBack = this.game.add.sprite(390, 0, 'ui'); //UI background
    this.uiBack.fixedToCamera = true; //fix to game camera
    
    this.uiRstBack = this.game.add.sprite(860, 0, 'ui'); //UI background
    this.uiRstBack.fixedToCamera = true; //fix to game camera
    
    this.uiMenuBack = this.game.add.sprite(-80, 0, 'ui'); //UI background
    this.uiMenuBack.fixedToCamera = true; //fix to game camera

    this.uiPush = this.game.add.text(width, height, pushText, style); //add to game
    this.uiScore = this.game.add.text(width, height + 25, scoreText, style); //add to game
    this.uiPush.fixedToCamera = true; //fix to camera
    this.uiScore.fixedToCamera = true; //fix to camera
    
    //Main Menu text   
    var menuText = "Main Menu \n F5",
        menuWidth = 10,
        menuHeight = 2;
    this.menu = this.game.add.text(menuWidth, menuHeight, menuText, style);
    this.menu.fixedToCamera = true;    
    
     //Restart text
    var rstText = "Restart \n Level",
        rstWidth = 910,
        rstHeight = 2;
    this.levelReset = this.game.add.text(rstWidth, rstHeight, rstText, style);
    this.levelReset.fixedToCamera = true; //fix to camera
    this.levelReset.inputEnabled = true;        
    this.levelReset.events.onInputDown.add(this.restart, this);
    this.levelReset.events.onInputOver.add(this.over, this);
    this.levelReset.events.onInputOut.add(this.out, this);
    
    var trickText = "Testing...",
        trickWidth = 465,
        trickHeight = 200,
        trickStyle = { font: "40px Patrick Hand", fill: "#ffffff", align: "center" };
    this.uiTrick = this.game.add.text(trickWidth, trickHeight, trickText, trickStyle);
    this.uiTrick.fixedToCamera = true;
    
    if (this.debug) {
        console.log("UI Setup");
    }
    
    return this;
};

UI.prototype = Object.create(Phaser.Sprite.prototype);
UI.prototype.constructor = UI;

UI.prototype = {

    updateText: function () {
        this.skater = this.game.getSkater(); //skater reference
        this.uiPush.setText(this.skater.getPushStatus());
        this.uiScore.setText("Score: " + this.skater.getScore());
    },
    
    trickMessage: function (_key) {
        
        if(_key != null) {
            switch (_key) {
                case 0: // Pump
                    this.uiTrick.setText("Pump!");
                    break;
                case 1: // Ollie Prime
                    this.uiTrick.setText("Ollie Primed!");
                    this.uiTrick.angle = 0;
                    break;
                case 2: // Ollie
                    this.uiTrick.setText("Ollie!");
                    this.uiTrick.angle = -10;
                    break;
                case 3: // Nollie Prime
                    this.uiTrick.setText("Nollie Primed!");
                    this.uiTrick.angle = 0;
                    break;
                case 4: // Nollie
                    this.uiTrick.setText("Nollie!");
                    this.uiTrick.angle = 10;
                    break;
                case 5: // Manual
                    this.uiTrick.setText("Manual!");
                    this.uiTrick.angle = -10;
                    break;
                case 6: // Nose Manual
                    this.uiTrick.setText("Nose Manual!");
                    this.uiTrick.angle = 10;
                    break;
                case 7: // Impossible
                    this.uiTrick.setText("Impossible!");
                    this.uiTrick.angle = -15;
                    break;
                case 8: // Hardflip
                    this.uiTrick.setText("Hardflip!");
                    this.uiTrick.angle = -15;
                    break;
                case 9: // Finish Message
                    this.uiTrick.setText("Finished! Score: " + this.skater.getScore());
                    this.uiTrick.angle = 0;
                    break;
                
                case 100:
                    this.uiTrick.setText("");  
                    break;
            }
        }
    },
    
    restart: function () {
        this.game.state.restart();
    },
    
    over: function () {
        var rstText = "Restart \n Level",
            rstWidth = 910,
            rstHeight = 2,
            overStyle = { font: "20px Fontdiner Swanky", fill: "#555", align: "center" };
        this.levelReset = this.game.add.text(rstWidth, rstHeight, rstText, overStyle);
        this.levelReset.fixedToCamera = true; //fix to camera
    },
    
    out: function () {
        var rstText = "Restart \n Level",
            rstWidth = 910,
            rstHeight = 2,
            outStyle = { font: "20px Fontdiner Swanky", fill: "#999", align: "center" };
        this.levelReset = this.game.add.text(rstWidth, rstHeight, rstText, outStyle);
        this.levelReset.fixedToCamera = true; //fix to camera
    }

};
