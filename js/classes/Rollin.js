Rollin = function (_game, _x, _y, _debug){

    this.game = _game; //game reference
    this.debug = _debug; //debug reference

    rollin = this.game.add.sprite(_x, _y, 'roll-in');

    this.game.physics.p2.enable(rollin);
        
    rollin.body.static = true;
    rollin.body.debug = this.debug;
    rollin.body.clearShapes(); //remove any other physics shapes
    rollin.body.addPolygon({},[ [0,0], [140,0], [260,50], [0,50] ]);

    rollin.body.setCollisionGroup(this.game.CG_ramps);

    if (this.debug){
        console.log("Roll-in Setup");
    }
  
    return rollin;
};

Rollin.prototype = Object.create(Phaser.Sprite.prototype);
Rollin.prototype.constructor = Rollin;
