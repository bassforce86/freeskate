Skater = function (_game, _x, _y, _debug, _ui) {

    this.game = _game; //Game reference
    this.UIClass = _ui; // UI class reference
    this.isDebug = _debug; //Change to enable debug for class.

    var backFoot, //back foot
        cursors, //arrow keys
        deck, //deck reference
        frontFoot, //front foot
        hardflip, //Hardflip key
        impossible, //impossible key
        pushButton, //button for jumping
        reset, //reset key
        trickTime, //time it takes for each trick
        wheel_back, //back wheel reference
        wheel_front, //front wheel reference
        jumpForce; //strength of jump
    
    
    //----Booleans----//
    this.check = false; // push check
    this.isTrick = false; //if skater is doing a trick
    this.isPushButtonDown = false; // push button check+
    this.backFootPrimed = false; //Prime back foot / Manual
    this.frontFootPrimed = false; //Prime front foot / Nose Manual
    
    //---Timer Settings---//
    this.angleTimer = 0; //time till deck angle reset 
    this.glideCounter = 0; //time between each set of 3 pushes
    this.jumpTimer = 0; //timer between jumps
    this.keyTimer = 0; //timer to check to jump key combo
    this.pushTime = 0; //time between pushes
    this.resetTimer = 0; //time between resets   
    this.trickTime = 0; //time trick takes to complete
    
    //----Tricks----//
    //Hardflip
    this.deck = this.game.add.sprite(_x, _y, 'skater', '0000.png');
    this.deck.animations.add('hardflip', 
                             [ '0000.png', '0009.png', '0023.png', '0030.png', '0045.png' ], 
                             10 /* frame rate */, 
                             false /* loop */, 
                             false /* kill on complete */);
    
    this.deck.animations.add('impossible', 
                             [ '0050.png', '0057.png', '0060.png', '0065.png', '0070.png', 
                              '0077.png','0075.png', '0080.png', '0084.png','0090.png' ],
                             15 /* frame rate */, 
                             false /* loop */, 
                             false /* kill on complete */);
        
    //----Speed Settings----//
    this.pushSpeed = 120; //current speed of wheel rotation per push
        
    //----Score----//
    this.score = 0; //score reference
    
    this.wheel_front = this.game.add.sprite( _x + 70, _y + 85);
    this.wheel_back = this.game.add.sprite( _x + 20, _y + 85);

    this.game.CG_board = this.game.physics.p2.createCollisionGroup();

    this.game.physics.p2.enable([this.wheel_front, this.wheel_back, this.deck ]);

    //Skate deck
    //this.deck.scale.setTo(0.6, 0.6);
    this.deck.body.clearShapes();
    this.deck.body.addPolygon({},[ [0,68], //Top Left (x, y)
                                   [15,74],
                                   [75,74], //Top Right
                                   [75,77], //Bottom Right
                                   [15,77] ]); //Bottom Left
    this.deck.anchor.y = 0.90; //reset vertical centre of mass
    this.deck.anchor.x = 0.47; //reset horizontal centre of mass
    this.deck.body.debug = this.isDebug; 
    this.deck.name = 'skater';

    //Front wheel
    this.wheel_front.body.setCircle(3);
    this.wheel_front.body.debug = this.isDebug;


    //Back wheel
    this.wheel_back.body.setCircle(3);
    this.wheel_back.body.debug = this.isDebug;

    //Spring(world, bodyA, bodyB, restLength, stiffness, damping, worldA, worldB, localA, localB)
    var springF = this.game.physics.p2.createSpring(this.deck, this.wheel_front, 0, 100, 30,null,null,[-20,0],null);
    var springB = this.game.physics.p2.createSpring(this.deck, this.wheel_back, 0, 100, 30 ,null,null,[20,0],null);

    //PrismaticConstraint(world, bodyA, bodyB, lockRotation, anchorA, anchorB, axis, maxForce)
    var constraintF =  this.game.physics.p2.createPrismaticConstraint(this.deck, this.wheel_front, false, [32, 6], [0, 0], [0, 1]);

    //Set limits
    constraintF.lowerLimitEnabled = constraintF.upperLimitEnabled = true;
    constraintF.upperLimit = 0;
    constraintF.lowerLimit = 0;

    var constraintB = this.game.physics.p2.createPrismaticConstraint(this.deck, this.wheel_back, false, [-22, 6], [0, 0], [0, 1]);

    //Set limits
    constraintB.lowerLimitEnabled = constraintB.upperLimitEnabled = true;
    constraintB.upperLimit = 0;
    constraintB.lowerLimit = 0; 
    
    //----Controls----//
    this.jumpForce = 300; //strength of jump    
    this.pushCount = 0; //max push count 3 - see method
    this.impossible = this.game.input.keyboard.addKey(65); // A key
    this.hardflip = this.game.input.keyboard.addKey(83); // S key
    this.cursors = this.game.input.keyboard.createCursorKeys(); // Manual buttons
    this.backFoot = this.game.input.keyboard.addKey(188); // < key
    this.frontFoot = this.game.input.keyboard.addKey(190);// > key
    this.pushButton = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    this.pushButton.onDown.add(this.push, this);
    
    this.trickState = false;
    this.scoreSet = false;
    this.trickName = "";
    
    //----Trick Message Legend----//
    // 0 = Push
    // 1 = Ollie Primed
    // 2 = Ollie
    // 3 = Nollie Primed
    // 4 = Nollie
    // 5 = Manual
    // 6 = Nose Manual
    // 7 = Impossible
    // 8 = Hardflip
    
    // 100 = blank
    //------End of Legend------//
    
    this.game.camera.follow(this.deck, Phaser.Camera.FOLLOW_PLATFORMER);
    
    if (this.isDebug) {
        console.log("Creating Skater");
    }
    
    return this;
};

Skater.prototype = Object.create(Phaser.Sprite.prototype);
Skater.prototype.constructor = Skater;

Skater.prototype = {
    
    movement: function () {
        
        this.manual = 40;
        this.UIClass.trickMessage(100);  
        
        if (this.checkManual() && this.checkNoseManual()) {
            // Prime Ollie
            if (this.backFoot.isDown && this.frontFoot.isUp) {   
                this.backFootPrimed = true;
                this.frontFootPrimed = false;
                this.UIClass.trickMessage(1);

            // Prime Nollie    
            } else if (this.backFoot.isUp && this.frontFoot.isDown) {     
                this.frontFootPrimed = true;
                this.backFootPrimed = false;
                this.UIClass.trickMessage(3);
            }
        }
        
        // Manual
        if (this.cursors.left.isDown && this.checkManual()) {
            if (Math.floor(this.wheel_front.body.y) > (Math.floor(this.wheel_back.body.y) - 8)) {
                this.wheel_front.body.moveUp(this.manual); 
            }
            this.UIClass.trickMessage(5);
        }
                          
        // Nose Manual
        if (this.cursors.right.isDown && this.checkNoseManual()) {
            if (Math.floor(this.wheel_back.body.y) > (Math.floor(this.wheel_front.body.y) - 8)) {
                this.wheel_back.body.moveUp(this.manual);
            }
            this.UIClass.trickMessage(6);
        }
        
        // Ollie
        if (this.backFoot.isDown && this.frontFoot.isDown && this.backFootPrimed) {
            this.ollie();
            this.UIClass.trickMessage(2);
        }
        
        // Nollie
        if (this.backFoot.isDown && this.frontFoot.isDown && this.frontFootPrimed) {
            this.nollie();
            this.UIClass.trickMessage(4);            
        }
        
        //----- Tricks -----//        
        if (!this.checkManual() && !this.checkNoseManual()) {
                    
            // Impossible Trick
            if (this.impossible.isDown) {
                this.impAnim = this.deck.animations.play('impossible');
                this.trickName = this.setTrickState(this.impAnim);
                this.UIClass.trickMessage(7);
            }

            // Hardflip Trick
            if (this.hardflip.isDown) {
                this.hrdAnim = this.deck.animations.play('hardflip');
                this.trickName = this.setTrickState(this.hrdAnim);
                this.UIClass.trickMessage(8);
            }
            
            if (this.isDebug) {
                console.log("Trick state: " + this.trickState); 
            }
            
            if (this.trickState) {                
                this.setTrickScore(this.trickName);
                if (this.scoreSet) {
                    this.trickState = false;
                }               
            }           
        }    
        
        if (this.scoreSet && this.checkManual() && this.checkNoseManual()) {
            this.scoreSet = false;
        }
    },
    
    setTrickState: function (_anim) {
        if (this.isDebug) {
            console.log(_anim.name + " is playing");
        }
        
        this.trickState = true; 
        return _anim;
    },
    
    setTrickScore: function (_anim) {
        if (this.isDebug) {
            console.log(_anim.name + " Score Added. State: " + this.scoreSet); 
        }
        
        if (_anim.isFinished && !this.scoreSet) {
            this.score += 15;
            this.scoreSet = true;
        }      
    },
    
    checkManual: function () {
        var yAxis = p2.vec2.fromValues(0, 1),
            result = { back: false },
            d;

        for (var i = 0; i < this.game.physics.p2.world.narrowphase.contactEquations.length; i++) {
              var c = this.game.physics.p2.world.narrowphase.contactEquations[i];
            
            if (c.bodyA === this.wheel_back.body.data || c.bodyB === this.wheel_back.body.data) {
                d = p2.vec2.dot(c.normalA, yAxis); // Normal dot Y-axis
                if (c.bodyA === this.wheel_back.body.data) d *= -1;
                if (d > 0.5) result.back = true;
            }
        }

        if(result.back){
            return true;
        } else {
            return false;
        }
    },
    
    checkNoseManual: function () {
        var yAxis = p2.vec2.fromValues(0, 1),
            result = { front: false },
            d;

        for (var i = 0; i < this.game.physics.p2.world.narrowphase.contactEquations.length; i++) {
              var c = this.game.physics.p2.world.narrowphase.contactEquations[i];
            
            if (c.bodyA === this.wheel_front.body.data || c.bodyB === this.wheel_front.body.data) {
                d = p2.vec2.dot(c.normalA, yAxis); // Normal dot Y-axis
                if (c.bodyA === this.wheel_front.body.data) d *= -1;
                if (d > 0.5) result.front = true;
            }
        }

        if(result.front){
            return true;
        } else {
            return false;
        }
    },
    
    push: function () {
        
        if (this.game.time.now > this.glideCounter) {
            if (this.game.time.now > this.pushTime) {
                if (this.pushCount < 5) {
                    this.pushSpeed += 30;
                    this.pushCount ++;
                }
                
                this.pushTime = this.game.time.now + 400;
                
                if (this.isDebug) {
                    console.log("push");    
                }
                
                this.UIClass.trickMessage(0);                
            }
            
            if (this.pushCount >= 5) {
                this.glideCounter = this.game.time.now + 3000;
                this.pushCount = 0;
                
                if (this.isDebug) {
                    console.log("reset push count");
                }
            }
        }
        
        this.wheel_front.body.angularVelocity = this.pushSpeed;
        this.wheel_back.body.angularVelocity = this.pushSpeed;
    },

    resetDeckAngle: function () {
        
        if (this.game.time.now < this.jumpTimer && this.game.time.now > this.angleTimer) {

            if (this.deck.body.angle.toFixed(1) < -1) {
                this.deck.body.angle += 5;
                this.wheel_back.body.y = this.wheel_front.body.y.toFixed(1);
                this.wheel_back.body.x = this.deck.body.x - 22;
                if (this.isDebug) {
                    console.log("Rotate forwards: " + this.deck.body.angle.toFixed(1));
                }                
            }

            if (this.deck.body.angle.toFixed(1) > 1) {
                this.deck.body.angle -= 5;
                this.wheel_front.body.y = this.wheel_back.body.y;
                this.wheel_front.body.x = this.deck.body.x + 22;
                if (this.isDebug) {
                    console.log("Rotate backwards: " + this.deck.body.angle.toFixed(1));
                }
            }
        }
    },

    ollie: function () {        
      
        if(this.checkManual() && this.jumpTimer < this.game.time.now){
            this.wheel_front.body.moveUp(this.jumpForce);
            this.wheel_back.body.moveUp(15 + this.jumpForce);
            this.deck.body.angle = -15;

            this.angleTimer = this.game.time.now + 400;
            this.jumpTimer = this.game.time.now + 600;
            this.score += 2;

            if (this.isDebug){
                console.log("Ollie");
            }
        }         
    },

    nollie: function () {

        if(this.checkNoseManual() && this.jumpTimer < this.game.time.now){
            this.wheel_front.body.moveUp(this.jumpForce);
            this.wheel_back.body.moveUp(20 + this.jumpForce);
            this.deck.body.angle = 15;
            
            this.angleTimer = this.game.time.now + 400;
            this.jumpTimer = this.game.time.now + 600;
            this.score += 5;

            if (this.isDebug) {
                console.log("Nollie");
            } 
        }
    },

    setScore: function (_score) {
        this.score = _score;
    },

    getScore: function () {
        return this.score;
    },

    getPushStatus: function () {
        var pushStatus;
        if(this.pushCount === 0 && this.game.time.now > this.glideCounter){
            pushStatus = "5 Pushes";
        } else if (this.pushCount == 1){
            pushStatus = "4 Pushes";
        } else if (this.pushCount == 2){
            pushStatus = "3 Pushes";
        } else if (this.pushCount == 3){
            pushStatus = "2 Pushes";
        } else if (this.pushCount == 4){
            pushStatus = "1 Push Left";
        } else {
            pushStatus = "Rollin'";
        }
        return pushStatus;
    },
    
    finished: function () {
        this.impossible.enabled = false;
        this.hardflip.enabled = false;
        this.cursors.enabled = false;
        this.backFoot.enabled = false;
        this.frontFoot.enabled = false;
    },
    
    destroy: function () {
        this.deck.kill();
        this.wheel_back.kill();
        this.wheel_front.kill();
    }
 
};
