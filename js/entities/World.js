
World = function (_game, _debug) {

    this.game = _game; //game reference
    this.debug = _debug; //debug reference

    this.game.world.setBounds(0, 0, 4800, 600);

    backdrop = this.game.add.tileSprite(0, -100, 4800, 1080, 'backdrop');
    this.game.CG_ground = this.game.physics.p2.createCollisionGroup();
    this.ground = this.game.add.tileSprite(2400, this.game.world.height, 4800, 72, 'ground');

    this.game.physics.p2.enable(this.ground);
    this.ground.body.setRectangle(4800, 32);
    this.ground.body.debug = this.debug;
    this.ground.body.static = true;
    this.ground.body.setCollisionGroup(this.game.CG_ground);

    this.finish = this.game.add.tileSprite(4650, 300, 150, 600, 'finish');
    this.game.physics.p2.enable(this.finish);

    finishY = this.game.world.height * 2;
    this.finish.body.setRectangle(150, finishY);
    this.finish.body.debug = this.debug;
    this.finish.body.static = true;
    this.finish.name = 'finish';
    
    if (this.debug){
        console.log("World setup");
    }

    return this;
};

World.prototype = Object.create(Phaser.Sprite.prototype);
World.prototype.constructor = World;

World.prototype = {
    
    getFinish: function () {
        return this.finish;
    }
}
