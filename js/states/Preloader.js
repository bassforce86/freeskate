var SkateGame;

SkateGame.Preloader = function (game) {
    "use strict";
    this.background = null;
    this.preloadBar = null;

    this.ready = false;

};

var console;

SkateGame.Preloader.prototype = {

    preload: function () {
        //	These are the assets we loaded in Boot.js
        //	A nice sparkly background and a loading progress bar

        this.background = this.add.tileSprite(0, 0, 1920, 1080, 'preloaderBackground');
        this.preloadBar = this.add.sprite(300, 400, 'preloaderBar');
        
        //Tricks

        this.backgroundColor = 0xffffff;

        this.time.advancedTiming = true;
        //	This sets the preloadBar sprite as a loader sprite.

        this.load.setPreloadSprite(this.preloadBar);

        //  Load the Google WebFont Loader script
        this.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');
        
        this.load.image('backdrop', 'assets/world/backdrop.png');
        this.load.image('ground', 'assets/world/ground.png');
        this.load.image('finish', 'assets/world/finish.png');

        this.load.image('roll-in', 'assets/ramps/Roll-in.png');
        this.load.image('ramp1', 'assets/ramps/basicjumpramp.png');
        this.load.image('ramp2', 'assets/ramps/ramp2.png');

        this.load.image('ui', 'assets/UI/uibackground.png');
        this.load.image('TitleButton','assets/UI/TitleButton.png');
        this.load.image('StartButton','assets/UI/StartButton.png');
        this.load.image('StartButtonMouseOver','assets/UI/StartButtonMouseOver.png');
        this.load.image('inGameMenu','assets/UI/inGameMenu.png');

        this.load.audio('moving', 'assets/SoundClips/SkateMove.wav');
        this.load.audio('ollie', 'assets/SoundClips/SkateOllie.wav');
        
        this.load.atlasJSONHash('skater', 'assets/skateboard/Textures/skater.png', 'assets/skateboard/SpriteSheets/skater.json');

    },

    create: function () {
        console.log("Creating preload state!");

        this.state.start('MainMenu');
    }

};
