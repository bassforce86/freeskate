var console;
var Phaser;
var SFX;

SFX = function (_game, _debug){

    this.game = _game; //game reference
    this.debug = _debug; //debug reference

    var sfxMove,
        sfxOllie,
        button;
    
    this.sfxMove = this.game.add.audio('moving', 1, true);
    this.sfxOllie = this.game.add.audio('ollie', 1, true);
    
    this.volDown = this.game.add.button(100, 100, 'Some Sprite', this.changeVolume, this, 2, 1, 0);

    console.log("SFX Created");
    
    return this;

};

SFX.prototype = Object.create(Phaser.Sprite.prototype);
SFX.prototype.constructor = SFX;

SFX.prototype = {

    playMoving: function() {
        this.sfxMove.play('moving', 0, 1, true);
    },

    playOllie: function() {
        this.sfxOllie.play('ollie', 0, 1, true);
    },

    changeVolume: function() {

    }

};
