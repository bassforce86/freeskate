SkateGame.MainMenu = function (game) {

    var titleBack,
        playButton,
        exitButton;
};

SkateGame.MainMenu.prototype = {

	create: function () {        
        
        this.background = 0xffffff;
        
        this.titleBack = this.add.sprite(this.world.centerX, 100, 'TitleButton');    
        this.titleBack.anchor.set(0.5, 0.5);
        this.startGameWidth = this.world.centerX;
        this.startGameHeight = 300;
        this.playButton = this.add.sprite(this.startGameWidth, this.startGameHeight, 'StartButton');
        this.playButton.anchor.set(0.5, 0.5);
                
        this.playButton.inputEnabled = true;        
        this.playButton.events.onInputDown.add(this.startGame, this);
        this.playButton.events.onInputOver.add(this.over, this);
        this.playButton.events.onInputOut.add(this.out, this);
        
        //Main Controls
        var text = "Controls:\n" + 
                    "Push: Space Bar\n\n" +
                    "Ollie Primer: <\n" + 
                    "Ollie: < + >\n\n" + 
                    "Nollie Primer: >\n" + 
                    "Nollie: > + <\n\n" + 
                    "Tricks:\n" + 
                    "Manual: Left Arrow\n" + 
                    "Nose Manual: Right Arrow\n\n" + 
                    "Impossible: S\n" + 
                    "Hardflip: A\n" +
                    "Reset Deck: R",
            style = { font: "20px Patrick Hand", fill: "#999", align: "left" },
        t = this.add.text(20, 100, text, style); //add controls text
        
        console.log("Creating menu state!");
    },
    
    over: function () {
        //Change sprite on mouse over
        this.playButton = this.add.sprite(this.startGameWidth, this.startGameHeight, 'StartButtonMouseOver');
        this.playButton.anchor.set(0.5, 0.5);
    },
    
    out: function () {
        //change sprite back to original sprite on mouse out
        this.playButton = this.add.sprite(this.startGameWidth, this.startGameHeight, 'StartButton');
        this.playButton.anchor.set(0.5, 0.5);
    },
    
    startGame: function () {
        //Start game state
        this.state.start('Game');
    }

};
