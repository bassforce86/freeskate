SkateGame.Game = function (game) {
    
    var backdrop, //world backdrop
        camera, //camera
        debug, //debug
        finish, //finish area
        ground, //platform for player
        skater, // new skater
        ui, //UI reference
        world, //world reference
        CG_board, //skateboard collision group
        CG_ramps, //Ramps collision group
        CG_ground; //Ground collision group
    
    this.debug = false; //global debug
};

SkateGame.Game.prototype = {

    create: function () {

        //Enable physics system
        this.physics.startSystem(Phaser.Physics.P2JS);
        console.log("Creating game state!");    
        
        //instantiate variables
        this.resetTimer = 0;

        //Setup Processes
        this.world = new World(this, this.debug);
        this.setupPhysics();
        this.ui = new UI(this, this.debug);
        this.skater = new Skater(this, 50, 255, this.debug, this.ui);
        this.setupRamps();
        this.setupControls();
        this.setupSkaterCollisions();
        this.physics.p2.updateBoundsCollisionGroup(); //update for all groups

        //Game settings
        this.physics.p2.setPostBroadphaseCallback(this.onFinish, this);
    },

    update: function () {

        //enable skater movement
        this.skater.movement();
        
        //Only allow reset once every 400 frames
        if (reset.isDown && this.resetTimer < this.time.now) {
            this.resetSkater(this.skater.deck.body.x, this.skater.deck.body.y);
            this.resetTimer = this.time.now + 600;
            this.setupRamps();
            this.setupSkaterCollisions();
        }

        //keep the UI text updated
        this.ui.updateText();

    },

    setupPhysics: function () {

        //  Set world bounds to world dimensions.
        this.physics.p2.setBoundsToWorld(true, true, true, true, false);

        this.physics.p2.setImpactEvents(true); //enable collisions
        this.physics.p2.restitution = 0.3; //set 'Bouncyness'
        this.physics.p2.gravity.y = 500; //set gravity
        this.physics.p2.friction = 1; //set world friction
        
        if (this.debug) {
            console.log("Physics enabled");
        }
    },
    
    setupControls: function () {

        reset = this.input.keyboard.addKey(82); //R key
        //Research Link http://www.cambiaresearch.com/articles/15/javascript-char-codes-key-codes
        
        if (this.debug) {
            console.log("Controls setup");
        }
        
    },

    setupRamps: function () {

        //Create ramp collision group
        this.CG_ramps = this.physics.p2.createCollisionGroup();

        var groundHeight = 535; //reference to ground height for ramps

        this.rollin = new Rollin(this, 0, groundHeight, this.debug);
        
        this.ramp1 = new Ramp1(this, 900, groundHeight, this.debug);
        this.ramp2 = new Ramp1(this, 1600, groundHeight, this.debug);
        this.ramp3 = new Ramp1(this, 3000, groundHeight, this.debug);
        
        //Ramp 2 is 25 pixels taller
        this.ramp4 = new Ramp2(this, 3800, groundHeight - 25, this.debug); 
        this.ramp5 = new Ramp2(this, 2400, groundHeight - 25, this.debug);

        //Setup Ramps Collsion Group
        this.rollin.body.setCollisionGroup(this.CG_ramps);
        this.ramp1.body.setCollisionGroup(this.CG_ramps);
        this.ramp2.body.setCollisionGroup(this.CG_ramps);
        this.ramp3.body.setCollisionGroup(this.CG_ramps);
        this.ramp4.body.setCollisionGroup(this.CG_ramps);
        this.ramp5.body.setCollisionGroup(this.CG_ramps);

        //setup each ramps collision with the skateboard
        this.rollin.body.collides(this.CG_board);
        this.ramp1.body.collides(this.CG_board);
        this.ramp2.body.collides(this.CG_board);
        this.ramp3.body.collides(this.CG_board);
        this.ramp4.body.collides(this.CG_board);
        this.ramp5.body.collides(this.CG_board);

    },

    setupSkaterCollisions: function () {

        //Setup Skater Collisions        
        this.skater.deck.body.setCollisionGroup(this.CG_board);        
        this.skater.wheel_front.body.setCollisionGroup(this.CG_board);
        this.skater.wheel_back.body.setCollisionGroup(this.CG_board);
        
        //collide with world boundries        
        this.skater.deck.body.collideWorldBounds = true;        
        this.skater.wheel_front.body.collideWorldBounds = true;
        this.skater.wheel_back.body.collideWorldBounds = true;
        
        //collide with other collision groups        
        this.skater.deck.body.collides([this.CG_ground, this.CG_ramps]);        
        this.skater.wheel_front.body.collides([this.CG_ground, this.CG_ramps]);
        this.skater.wheel_back.body.collides([this.CG_ground, this.CG_ramps]);

        //Setup Ground Collisions
        this.world.ground.body.setCollisionGroup(this.CG_ground);
        this.world.ground.body.collides(this.CG_board);

        if (this.debug) {
            console.log("Collision Groups Setup");
        }
        
    },
    
    resetSkater: function (_x, _y) {
        var currentScore = this.skater.getScore();    
        var currentX = _x;
        var currentY = _y;
        this.skater.destroy();
        console.log("Reset");
        this.skater = new Skater(this, (currentX - 40), (currentY - 120), this.debug, this.ui);
        this.skater.setScore(currentScore);
    },

    onFinish: function (body1, body2) {
        
        if (this.debug) {
            console.log(body1.sprite.name + " : " + body2.sprite.name);
        }        
        
        if ((body1.sprite.name === 'skater' && body2.sprite.name === 'finish') || 
            (body2.sprite.name === 'skater' && body1.sprite.name === 'finish')) {
            
            this.skater.finished();
            this.ui.trickMessage(9);
            return false;
        }
        return true;
        
        
        this.skater.brake();
    },

    getSkater: function () {
        //To pass around the same skater object
        return this.skater;
    },

    render: function() {
        //rendering Frames per Second
        if (this.debug) {
            this.game.debug.text(this.game.time.fps || '--', 590, 14, "#000000");
        }
        
    },

    quitGame: function (pointer) {

        //	Here you should destroy anything you no longer need.
        //	Stop music, delete sprites, purge caches, free resources, all that good stuff.

        //	Then let's go back to the main menu.
        this.state.start('MainMenu');
    }
};
