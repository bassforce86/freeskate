Ramp2 = function (_game, _x, _y, _debug){

    this.game = _game; //game reference
    this.debug = _debug; //debug reference

    ramp = this.game.add.sprite(_x,_y, 'ramp2');

    this.game.physics.p2.enable(ramp);

    ramp.body.static = true;
    ramp.body.debug = false;
    ramp.body.clearShapes();

    var deckHeight = 30;
    var rampHeight = 75;
    var poleHeight = 10;
    var poleStart = 100;
    var poleEnd = 320;

    ramp.body.addPolygon({},[ [-10,rampHeight],
                            [80,deckHeight],[poleStart,deckHeight],
                            [poleStart,poleHeight],[poleEnd,poleHeight],
                            [poleEnd,deckHeight],[350,deckHeight],
                            [435,65],[470,rampHeight] ]);

    if (this.debug) {
        console.log("Ramp 2 Setup");
    }

    return ramp;
};

Ramp2.prototype = Object.create(Phaser.Sprite.prototype);
Ramp2.prototype.constructor = Ramp2;
