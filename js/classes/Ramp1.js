Ramp1 = function (_game, _x, _y, _debug){

    this.game = _game; //game reference
    this.debug = _debug;

    ramp = this.game.add.sprite(_x,_y, 'ramp1');

    this.game.physics.p2.enable(ramp);

    ramp.body.static = true;
    ramp.body.debug = this.debug;
    ramp.body.clearShapes();
    ramp.body.addPolygon({},[ [0,50], [45,40], [85,20], [112,0], [255,0], [450,50], [180,40] ]);

    if (this.debug) {
        console.log("Ramp 1 Setup");
    }
    
    return ramp;

};

Ramp1.prototype = Object.create(Phaser.Sprite.prototype);
Ramp1.prototype.constructor = Ramp1;
